# index.js
const {Client} = require('pg');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.json())
app.use(
  bodyparser.urlencoded({
    extended: true,
  })
)

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'employeedb',
    password: '1234',
    port: 5432,
  })
  client.connect()

  client.query('SELECT NOW()', (err, res) => {
    if(!err)
     console.log('DB connection succeded ....');
     else
     console.log('DB connection failed\n Error: '+ JSON.stringify(err, undefined, 2));
  })
  
  app.listen(3000,()=>console.log('Express server running at port no: 3000'));

  //1.Employer login 
  app.post('/employeeLogin',(req,res) =>{
        let emp = req.body;
        var pgsql = "SET @name = ?;SET @empid = ?;SET @lname = ?;SET @salary = ?;\
        CALL EmployeeLogin(@empid,@password);";
        client.query(pgsql, [emp.empid, emp.password], (err, result, fields) =>{
            if(!err)
                  res.send('Employee Login Successfully');
            else
                  res.send('Invalid ID or Password');
        })
    });

  //2.Employer can add new employee
  app.post('/addEmployee',(req,res) =>{
      let emp = req.body;
      var pgsql = "SET @name = ?;SET @empid = ?;SET @lname = ?;SET @salary = ?;SET @password = ?;\
      CALL EmployeeAddOrEdit(@name,@empid,@lname,@salary);";
      client.query(pgsql, [emp.name, emp.empid, emp.lname, emp.salary, emp.password], (err, result, fields) =>{
          if(!err)
            result.forEach(element =>{
                if(element.constructor == Array)
                res.send('Inserted employee id :'+element[0].empid);
            });
          else
                res.send('Something went wrong !!');
      })
  });
  
  //3.Employer can update any employee
  app.put('/updateEmployee',(req,res) =>{
    let emp = req.body;
    var pgsql = "SET @name = ?;SET @empid = ?;SET @lname = ?;SET @salary = ?;\
    CALL EmployeeAddOrEdit(@name,@empid,@lname,@salary);";
    client.query(pgsql, [emp.name, emp.empid, emp.lname, emp.salary], (err, result, fields) =>{
        if(!err)
              res.send('Updated Successfully..');
        else
              res.send('Something went wrong !!');
    })
});

  //4.Employer can remove an employee
  app.delete('/employee/:empid',function(req,res){
    client.query('DELETE FROM employee WHERE empid = ?',[req.param.empid],function(err,result,fields){
        if(!err)
            res.send('Employee Delete Successfully..');
        else
            res.send('Something went wrong !!');
    }) 
 });

 //5.Employer can increase or decrease rating of the employee
 app.post('/rating',(req,res) =>{
    let emp = req.body;
    var pgsql = 'UPDATE employee SET rating = ? WHERE empid = ?';
    client.query(pgsql, [emp.empid, emp.rating], (err, result, fields) =>{
        if(!err)
              res.send('Rating Done..');
        else
              res.send('Something went wrong !!');
    })
});
